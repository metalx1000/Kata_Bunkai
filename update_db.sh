#!/bin/bash
###################################################################### 
#Copyright (C) 2018  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

csv_url="https://docs.google.com/spreadsheets/d/e/2PACX-1vQcyBQFgeYINHEFxturTDoVkKTXbRqJIc0v-Toq5qWVlwjbVgHqpvR7YqBc704Th7hHkX9CJ_fgay-K/pub?gid=1932260448&single=true&output=csv"

function main(){
  echo "Updating..."
  wget -qO blackbelt.csv "$csv_url"
  csv2json -i blackbelt.csv -o blackbelt.json -f pretty 
  exit 0
}

main

