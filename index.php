<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="libs/jquery-3.3.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="libs/howler.core.js"></script>

<style>
.card,.card-title,#rank{
  margin:10px;
}

#random,#timer{
  margin-top:10px;
  margin-bottom:10px;
}

#random{
  margin-left:10px;
}

#timer{
  margin-right:10px;
}

.card-title{
  margin-right:60%;
}

.btn {
    margin-right: 2px;
}
</style>
<!--Fav icon-->
<?php include("fav.php");?>
<?php include("social.php");?>
<!--fav icon end-->


<script>
var belts = [];
var katas = [];
var rkatas = [];
var Intervals;
var alarm;

$(document).ready(function(){
  getInfo();
  $("#random").click(randomKata);
  $("#timer").click(timer);

  load_sounds();
});

function load_sounds(){
  alarm = new Howl({src: ['sounds/alarm.webm', 'sounds/alarm.mp3', 'sounds/alarm.wav']});
}

function getInfo(){
  $("#main").html("");
  $.post("blackbelt.json",function(data){
    data.forEach(function(i){
      belts.push(i.belt);
      belt = i.belt.toUpperCase();
      belts = sort_unique(belts);

      katas.push(i.kata.toUpperCase());
      katas = sort_unique(katas);
      rkatas = sort_unique(katas);

      if( i.time != ''){
        var time =` 
          <div class="col">
          <span>Should take <b>` + i.time + `</b> to perform</span>
          </div>`;
}else{
  var time = "";
}

color = check_belt(i);
info_link = check_info_link(i);
bunkiaBTN = check_bunkia(i);
const output = `<div class="card `+belt+` `+color+` kata">
  <div class="card-header"><h3>`+
  i.kata.toUpperCase() +`</h3>  
  </div>
  <div class="card-body">

  <div class="container">
  <div class="row">
  <div class="col">
  <span>`+i.translation+`</span>
  </div>
  `+time+`
  </div>
  </div>


  <hr>
  `+info_link+`
  <div class="btn-group d-flex" role="group">
  <a href="`+i.video1+`" class="btn btn-primary w-100 btn-space">Kata</a>
  `+bunkiaBTN+`
  </div> 
  </div> 
  </div>`;
$("#main").append(output);
    });
  }).done(addRank);
}

function speak(m){
  var msg = new SpeechSynthesisUtterance();
  var voices = window.speechSynthesis.getVoices();
  msg.text = m;
  msg.lang = 'en-US';
  //msg.lang = 'ja-JP';
  speechSynthesis.speak(msg);

}

function randomKata(){
  var kata = rkatas[Math.floor(Math.random() * rkatas.length)];
  //Remove from array
  var index = rkatas.indexOf(kata);
  if (index > -1) {
    rkatas.splice(index, 1);
    var len = rkatas.length;
  }

  if(len <= 0){
    rkatas = sort_unique(katas);
  }

  //display random kata
  $(".kata").hide();
  $( ".kata:contains('"+kata+"')" ).show();

  //speak the kata outloud
  //speak(kata);

  return true;
}

function filter(){
  var q = $(this).html();
  if( q == "ALL" ){
    $(".kata").show();
  }else{
    $(".kata").hide();
    $("."+q).show();
  }
}

function addRank(){
  $("#rank").html("<a class='btn w-100 btn-secondary rankbtn'>ALL</a>");
  belts.forEach(function(b){
    if(b != undefined){
      b = b.toUpperCase();
      if( b == "SHO-DAN" ){
        var color = "btn-danger text-white";
      }else if( b == "NI-DAN" ){
        var color = "btn-warning text-white";
      }else if( b == "SAN-DAN" ){
        var color = "btn-info text-white";
      }else{
        var color = "btn-light";
      }

      $("#rank").append("<a class='btn " + b + " " +color+" w-100 rankbtn'>"+b+"</a>");
    }
  });
  $(".rankbtn").click(filter);

}

function check_belt(i){
  var belt = i.belt.toLowerCase();
  if( belt == "sho-dan" ){
    var color = "border-danger";
  }else if( belt == "ni-dan" ){
    var color = "border-warning";
  }else if( belt == "san-dan" ){
    var color = "border-info";
  }else{
    var color = "bg-light";
  }

  return color;
}

function check_bunkia(i){
  if(i.bunkia1 != ""){
    bunkia1 = '<a href="'+i.bunkia1+'" class="btn btn-success w-100">Bunkia #1</a>';
  }else{
    bunkia1 = "";
  }

  if(i.bunkia2 != ""){
    bunkia2 = '<a href="'+i.bunkia2+'" class="btn btn-success w-100">Bunkia #2</a>';
  }else{
    bunkia2 = "";
  }

  return bunkia1 + bunkia2;
}

function check_info_link(i){
  if(i.info_link != ""){
    var info_link = `<h6>`+i.info+`<br><br><a class="btn btn-primary" href="`+i.info_link+`">More</a></h6>`;
  }else{
    var info_link = "";
  }

  return info_link;
}

function sort_unique(arr) {
  if (arr.length === 0) return arr;
  arr = arr.sort(function (a, b) { return a*1 - b*1; });
  var ret = [arr[0]];
  for (var i = 1; i < arr.length; i++) { //Start loop at 1: arr[0] can never be a duplicate
    if (arr[i-1] !== arr[i]) {
      ret.push(arr[i]);
    }
  }
  return ret;
}

function timer(){
  speak("Timer enabled. Starting in 10 seconds.");
  clearInterval(Intervals);
  $("#timer").html("Start Timer");
  setTimeout(function(){
    speak("Starting in 5 seconds.");
  },8000);

  setTimeout(function(){
    speak("Begin.");
    start_timer();
  },13000);
}

function start_timer(){
  var StartTime = new Date().getTime();
  Intervals = setInterval(function(){
    var time = new Date().getTime();
    time = getTimeDiff(StartTime,time);
    update_timer(time);
  },1000);
}

function update_timer(time){
  $("#timer").html(time);
  if(time == "0:30"){
    speak("30 Seconds");
  }else if(time == "1:00"){
    speak("1 Minute");
  }else if(time == "1:30"){
    speak("1 Minute 30 Seconds");
  }else if(time == "2:00"){
    speak("2 Minutes");
  }else if(time == "2:30"){
    speak("2 Minutes 30 Seconds");
  }else if(time == "2:31"){
    alarm.play(); 
  }else if(time == "2:45"){
    clearInterval(Intervals);
    $("#timer").html("Start Timer");
  }
}

function getTimeDiff(start, stop){
  var diff = (stop-start);
  var hours = Math.floor( diff / (1000*60*60) );
  var mins  = Math.floor( diff / (1000*60) );
  var secs  = Math.floor( diff / 1000 );

  if(secs >= 60){
    //console.log(secs);
    var div = mins * 60;
    secs = secs - div;
  }
  if(secs<10){secs = "0"+secs};
  return mins + ":" + secs;
}

</script>
  </head>
  <body>
    <div id="buttons" class="container">
      <div class="btn-group d-flex" id="rank" role="group"></div>
    </div>
    <div class="container">
      <div class="btn-group d-flex" role="group">
        <a class='btn btn-primary text-white w-100' id="random">Select Random Kata</a>
        <a class='btn btn-info text-white w-100' id="timer" >Start Timer</a>
      </div>
    </div>
        <hr>
    <div id="main" class="container">
    </div>

  </body>
</html>
